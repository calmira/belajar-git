const red = document.querySelector('.red')
red.addEventListener('click', function(){
    red.style.backgroundColor = "red"
})

const blue = document.querySelector('.blue')
blue.addEventListener('click', function(){
    blue.style.backgroundColor = "blue"
})

const green = document.querySelector('.green')
green.addEventListener('click', function(){
    green.style.backgroundColor = "green"
})

const reset = document.querySelector('.reset')
reset.addEventListener('click', function(){
    window.location.reload()
})